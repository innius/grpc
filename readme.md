# innius gRPC

grpc utils for innius

## Required Dependencies

* make
* protoc-gen-go
* follow installation instructions at [protobuf github page](https://github.com/protocolbuffers/protobuf) (note: [yay](https://github.com/Jguer/yay) is not mentioned but can install protobuf)
* follow instructions for use of `protoc` at [grpc-gateway readme documentation](https://github.com/grpc-ecosystem/grpc-gateway?tab=readme-ov-file)

## Use

See Makefile for all options.

### Updating a service API

1. Update the .proto file of the service in question
2. Run `make <service-ref>`, e.g. `make notifications` for generating grpc go-code for the push-notifications-api, or `make monitoring` for the monitoring-service-v3
