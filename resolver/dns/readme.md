# innius dns resolve 

gRPC dns resolver for innius service. 

The standard grpc dns resolver does not handle [custom SRV records][1]

This handler is inspired by the [standard go grpc dns resolver][2].
This dns resolver resolves DNS SRV records for innius services only. 

Records with service name '_grpc' are taken into account. 

This means each grpc based microservice must register itself with an SRV record like this 
```
_grpc._tcp.chair.innius.local,localhost,50051  
```
in order to avoid collisions with standard dns resolver this package uses a different scheme: 
```
address := "innius:///foo.bar" 
```

*Note 02-07-2021* watcher func has been aligned with official grpc dns resolver package. Some additional internal packages 
were added to this repository:  grpcrand and backoff. 

*Warning* maintaining functionality of this resolver in the context of future grpc versions might require updating this package, aligning it with the latest official grpc resolver changes. 

## Usage
*Warning* previously this package required a silent import which has been removed. For now it is required to set the resolver explicitly. 
 
```go
import  dnsresolver "bitbucket.org/innius/grpc/resolver/dns"

address := "innius:///foo.bar"
conn, err := grpc.Dial(address, grpc.WithInsecure(), grpc.WithResolvers(dnsresolver.NewBuilder()))
if err != nil {
  log.Fatalf("did not connect: %v", err)	
}
```


[1]:https://github.com/grpc/grpc-go/issues/2209
[2]:https://github.com/grpc/grpc-go/blob/master/internal/resolver/dns/dns_resolver.go
