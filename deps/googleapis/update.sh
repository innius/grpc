#!/bin/bash

# note: to keep things nice and simple, this script assumes it is run from its parent directory.

URL=https://raw.githubusercontent.com/googleapis/googleapis/master/google/api
LOC=./google/api

wget -O ${LOC}/annotations.proto ${URL}/annotations.proto
wget -O ${LOC}/field_behavior.proto ${URL}/field_behavior.proto
wget -O ${LOC}/http.proto ${URL}/http.proto
wget -O ${LOC}/httpbody.proto ${URL}/httpbody.proto
