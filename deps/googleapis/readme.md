# googleapis

The .proto files in this package were copied from the [googleapis github page](https://github.com/googleapis/googleapis/tree/master/google/api), as per instruction for usage of [grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway?tab=readme-ov-file), a protobuf extension that generates a RESTful API implementation from a protobuf definition.

## Maintenance

These files require manual maintenance, i.e. they need to be manually copied from the official googleapis repository if they need updating.
You can use the `update.sh` script to retrieve the files as follows:

```sh
cd ../local/path/to/googleapis
./update.sh
```

## Debugging

Always follow the above instructions for updating the files and retry before proceeding.

### Missing files

If the protobuf compiler complains about missing files or definitions, check the [grpc-gateway documentation](https://github.com/grpc-ecosystem/grpc-gateway?tab=readme-ov-file) at the section for using custom annotations for instructions on including the necessary dependencies for use with `protoc`.

If files still need to be supplied manually, ensure that all the required `.proto` files in mentioned github repository's directory (currently [this](https://github.com/googleapis/googleapis/tree/master/google/api)) are included in `update.sh` script.

### 404 URL Response

If the URL returns a 404, then the maintainers of the [googleapis](https://github.com/grpc-ecosystem/grpc-gateway?tab=readme-ov-file) have probably changed either the location of the files. Please check the [grpc-gateway documentation](https://github.com/grpc-ecosystem/grpc-gateway?tab=readme-ov-file) at the section for using custom annotations for instructions on including the necessary dependencies for use with `protoc`.
