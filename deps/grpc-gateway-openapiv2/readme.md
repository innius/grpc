# grpc-gateway-openapiv2

The .proto files in this package were copied from the [grpc-gateway github repository](https://github.com/grpc-ecosystem/grpc-gateway/tree/main/protoc-gen-openapiv2/options), as per instruction for usage of [grpc-gateway/protoc-gen-openapiv2](https://github.com/grpc-ecosystem/grpc-gateway?tab=readme-ov-file), a protobuf extension that generates a RESTful API openapiv2 swagger file from a protobuf definition.

## Maintenance

These files require manual maintenance, i.e. they need to be manually copied from the official googleapis repository if they need updating.
You can use the `update.sh` script to retrieve the files as follows:

```sh
cd ../local/path/to/grpc-gateway-openapiv2
./update.sh
```

## Debugging

Always follow the above instructions for updating the files and retry before proceeding.

### Missing files

If the protobuf compiler complains about missing files or definitions, check the [grpc-gateway documentation](https://github.com/grpc-ecosystem/grpc-gateway?tab=readme-ov-file) at the section for generating openapiv2 or swagger definitons for instructions on including the necessary dependencies for use with `protoc`.

If files still need to be supplied manually, ensure that all `.proto` files in github repository's openapiv2 directory (currently [here](https://github.com/grpc-ecosystem/grpc-gateway/tree/main/protoc-gen-openapiv2/options)) are included in `update.sh` script.

### 404 URL Response

If the URL returns a 404, then the maintainers of the [grpc-gateway](https://github.com/grpc-ecosystem/grpc-gateway?tab=readme-ov-file) have probably changed either the location of the files, or changed the requirements for their extension to protobuf. Please check their documentation at the section for generating openapiv2 or swagger definitions for instructions on including the necessary dependencies for use with `protoc`.
