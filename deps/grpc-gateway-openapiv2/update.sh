#!/bin/bash

# note: to keep things nice and simple, this script assumes it is run from its parent directory.

URL=https://raw.githubusercontent.com/grpc-ecosystem/grpc-gateway/main/protoc-gen-openapiv2/options
LOC=./protoc-gen-openapiv2/options

wget -O ${LOC}/annotations.proto ${URL}/annotations.proto
wget -O ${LOC}/openapiv2.proto ${URL}/openapiv2.proto
