monitoring:
	protoc \
		-I=./services/monitoring \
		-I=./deps/googleapis \
		-I=./deps/grpc-gateway-openapiv2 \
		--go_out=./services/monitoring --go_opt paths=source_relative \
		--go-grpc_out=./services/monitoring --go-grpc_opt paths=source_relative \
		--grpc-gateway_out=./services/monitoring --grpc-gateway_opt paths=source_relative \
		--openapiv2_out=./services/monitoring \
		./services/monitoring/service.proto
	go generate ./services/monitoring


notifications:
	protoc \
		-I=./services/push_notifications \
		-I=./deps/googleapis \
		-I=./deps/grpc-gateway-openapiv2 \
		--go_out=./services/push_notifications --go_opt paths=source_relative \
		--go-grpc_out=./services/push_notifications --go-grpc_opt paths=source_relative \
		--grpc-gateway_out=./services/push_notifications --grpc-gateway_opt paths=source_relative \
		--openapiv2_out=./services/push_notifications \
		./services/push_notifications/service.proto
	go generate ./services/push_notifications


