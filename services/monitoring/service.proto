syntax = "proto3";
package monitoringapi;

import "google/api/annotations.proto";
import "protoc-gen-openapiv2/options/annotations.proto";

option go_package = "bitbucket.org/innius/monitoringapi";

option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_swagger) = {
	info: {
		title: "Monitoring Service V3";
		description: "innius machine / sensor state api";
		version: "1.0.0";
		contact: {
			name: "innius";
			url: "https://innius.com";
			email: "info@innius.com";
		};
		license: {
			name: "API Terms of Service";
			url: 'https://innius.com/api-terms/';
		};
	};
	// Overwriting host entry breaks tests, so this is not done here.
	external_docs: {
		url: "https://bitbucket.org/innius/monitoring-service-v3";
		description: "service repository";
	}
	schemes: HTTPS;
	consumes: "application/json";
	produces: "application/json";
	security_definitions: {
		security: {
			key: "ApiKeyAuth";
			value: {
				type: TYPE_API_KEY;
				in: IN_HEADER;
				name: "X-API-Key";
			}
		}
	}
	security: {
		security_requirement: {
			key: "ApiKeyAuth";
			value: {};
		}
	}
	responses: {
		key: "403";
		value: {
			description: "You don't have access to the specified sensor(s)";
		}
	}
};

message SensorValueRequest {
	string machine_id = 1 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "trn of the machine"}];
	string sensor_id = 2 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "sensor id", required: ['sensor_id']}];
}

message SensorValueReply {
    int64 timestamp = 1;
    double last_value = 2;
}

message ListSensorStateRequest {
    string machine_id = 1 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "trn of the machine", required: ['machine_id']}];
    repeated string sensor_ids = 2 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "list of sensor ids", required: ['sensor_ids']}];
}

enum State {
    Normal = 0;
    Caution = 1;
    Emergency = 2;
}

message GetSensorStateRequest {
	string machine_id = 1 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "trn of the machine"}];
	string sensor_id = 2 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "sensor id", required: ['sensor_id']}];
}

message SensorState {
    int64 timestamp = 1 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "timestamp of the last sensor value" }];
    double last_value = 2 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "last known value of the sensor" }];
    State state = 3 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "the state of the sensor" }];
}

message ListSensorStateReply {
    map<string, SensorState> values = 1;
}

message MachineStateRequest {
    string machine_id = 1 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "trn of the machine"}];
}

message MachineStateReply {
    int64 timestamp = 1 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "timestamp of the last state change" }];
    State state = 2 [(grpc.gateway.protoc_gen_openapiv2.options.openapiv2_field) = {description: "the state of the machine" }];
}

service MonitoringService {
    rpc GetSensorValue (SensorValueRequest) returns (SensorValueReply) {
        option (google.api.http) = {
            get: "/monitoring-service-v3/{machine_id}/sensors/{sensor_id}/value"
        };
        option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
			description: "Get the last value of a sensor";
            summary: "Get Sensor Value";
            tags: "sensor";
        };
    }
	rpc GetSensorState(GetSensorStateRequest) returns (SensorState) {
		option (google.api.http) = {
			get: "/monitoring-service-v3/{machine_id}/sensors/{sensor_id}/state"
		};
		option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
			description: "Get the state of the specified sensors";
			summary: "Get Sensor State";
			tags: "sensor";
		};
	}

    rpc ListSensorState (ListSensorStateRequest) returns (ListSensorStateReply) {
        option (google.api.http) = {
            post: "/monitoring-service-v3/{machine_id}/state/sensors"
            body: "*"
        };
        option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
			description: "Get the state for a list of sensors";
			summary: "List Sensor States";
			tags: "sensor";
		};
    }
    rpc GetMachineState (MachineStateRequest) returns (MachineStateReply) {
        option (google.api.http) = {
            get: "/monitoring-service-v3/{machine_id}/state"
        };
        option (grpc.gateway.protoc_gen_openapiv2.options.openapiv2_operation) = {
			description: "Get the state of a machine";
			summary: "Get Machine State";
			tags: "machine";
			responses: {
				key: "404";
				value: {
					description: "Specified machine does not exist";
				}
			}
			responses: {
				key: "403";
				value: {
					description: "You don't have access to the specified machine";
				}
			}
		};
    }
}
