package monitoringapi 

const (
swagger = `{
  "swagger": "2.0",
  "info": {
    "title": "Monitoring Service V3",
    "description": "innius machine / sensor state api",
    "version": "1.0.0",
    "contact": {
      "name": "innius",
      "url": "https://innius.com",
      "email": "info@innius.com"
    },
    "license": {
      "name": "API Terms of Service",
      "url": "https://innius.com/api-terms/"
    }
  },
  "tags": [
    {
      "name": "MonitoringService"
    }
  ],
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/monitoring-service-v3/{machineId}/sensors/{sensorId}/state": {
      "get": {
        "summary": "Get Sensor State",
        "description": "Get the state of the specified sensors",
        "operationId": "MonitoringService_GetSensorState",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/monitoringapiSensorState"
            }
          },
          "403": {
            "description": "You don't have access to the specified sensor(s)",
            "schema": {}
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "machineId",
            "description": "trn of the machine",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "sensorId",
            "description": "sensor id",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "sensor"
        ]
      }
    },
    "/monitoring-service-v3/{machineId}/sensors/{sensorId}/value": {
      "get": {
        "summary": "Get Sensor Value",
        "description": "Get the last value of a sensor",
        "operationId": "MonitoringService_GetSensorValue",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/monitoringapiSensorValueReply"
            }
          },
          "403": {
            "description": "You don't have access to the specified sensor(s)",
            "schema": {}
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "machineId",
            "description": "trn of the machine",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "sensorId",
            "description": "sensor id",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "sensor"
        ]
      }
    },
    "/monitoring-service-v3/{machineId}/state": {
      "get": {
        "summary": "Get Machine State",
        "description": "Get the state of a machine",
        "operationId": "MonitoringService_GetMachineState",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/monitoringapiMachineStateReply"
            }
          },
          "403": {
            "description": "You don't have access to the specified machine",
            "schema": {}
          },
          "404": {
            "description": "Specified machine does not exist",
            "schema": {}
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "machineId",
            "description": "trn of the machine",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "machine"
        ]
      }
    },
    "/monitoring-service-v3/{machineId}/state/sensors": {
      "post": {
        "summary": "List Sensor States",
        "description": "Get the state for a list of sensors",
        "operationId": "MonitoringService_ListSensorState",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/monitoringapiListSensorStateReply"
            }
          },
          "403": {
            "description": "You don't have access to the specified sensor(s)",
            "schema": {}
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "machineId",
            "description": "trn of the machine",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "type": "object",
              "properties": {
                "sensorIds": {
                  "type": "array",
                  "items": {
                    "type": "string",
                    "required": [
                      "sensor_ids"
                    ]
                  },
                  "description": "list of sensor ids"
                }
              }
            }
          }
        ],
        "tags": [
          "sensor"
        ]
      }
    }
  },
  "definitions": {
    "monitoringapiListSensorStateReply": {
      "type": "object",
      "properties": {
        "values": {
          "type": "object",
          "additionalProperties": {
            "$ref": "#/definitions/monitoringapiSensorState"
          }
        }
      }
    },
    "monitoringapiMachineStateReply": {
      "type": "object",
      "properties": {
        "timestamp": {
          "type": "string",
          "format": "int64",
          "description": "timestamp of the last state change"
        },
        "state": {
          "$ref": "#/definitions/monitoringapiState",
          "description": "the state of the machine"
        }
      }
    },
    "monitoringapiSensorState": {
      "type": "object",
      "properties": {
        "timestamp": {
          "type": "string",
          "format": "int64",
          "description": "timestamp of the last sensor value"
        },
        "lastValue": {
          "type": "number",
          "format": "double",
          "description": "last known value of the sensor"
        },
        "state": {
          "$ref": "#/definitions/monitoringapiState",
          "description": "the state of the sensor"
        }
      }
    },
    "monitoringapiSensorValueReply": {
      "type": "object",
      "properties": {
        "timestamp": {
          "type": "string",
          "format": "int64"
        },
        "lastValue": {
          "type": "number",
          "format": "double"
        }
      }
    },
    "monitoringapiState": {
      "type": "string",
      "enum": [
        "Normal",
        "Caution",
        "Emergency"
      ],
      "default": "Normal"
    },
    "protobufAny": {
      "type": "object",
      "properties": {
        "@type": {
          "type": "string"
        }
      },
      "additionalProperties": {}
    },
    "rpcStatus": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        },
        "details": {
          "type": "array",
          "items": {
            "type": "object",
            "$ref": "#/definitions/protobufAny"
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "ApiKeyAuth": {
      "type": "apiKey",
      "name": "X-API-Key",
      "in": "header"
    }
  },
  "security": [
    {
      "ApiKeyAuth": []
    }
  ],
  "externalDocs": {
    "description": "service repository",
    "url": "https://bitbucket.org/innius/monitoring-service-v3"
  }
}
`
)
