package notificationapi 

const (
swagger = `{
  "swagger": "2.0",
  "info": {
    "title": "Push Notification API",
    "description": "push notification api for innius",
    "version": "1.0.0",
    "contact": {
      "name": "innius",
      "url": "https://innius.com",
      "email": "info@innius.com"
    },
    "license": {
      "name": "API Terms of Service",
      "url": "https://innius.com/api-terms/"
    }
  },
  "tags": [
    {
      "name": "PushNotificationApi"
    }
  ],
  "schemes": [
    "https"
  ],
  "consumes": [
    "application/json"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/push-notification-api/devices": {
      "put": {
        "operationId": "PushNotificationApi_RegisterUserDevice",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/notificationapiRegisterUserDeviceReply"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/notificationapiRegisterUserDeviceRequest"
            }
          }
        ],
        "tags": [
          "PushNotificationApi"
        ]
      },
      "patch": {
        "operationId": "PushNotificationApi_DeregisterUserDevice",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/notificationapiDeregisterUserDeviceReply"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/notificationapiDeregisterUserDeviceRequest"
            }
          }
        ],
        "tags": [
          "PushNotificationApi"
        ]
      }
    },
    "/push-notification-api/devices/{registrationToken}/check": {
      "post": {
        "operationId": "PushNotificationApi_CheckUserDeviceRegistration",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/notificationapiCheckUserDeviceRegistrationReply"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "registrationToken",
            "description": "device registration token",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "PushNotificationApi"
        ]
      }
    },
    "/push-notification-api/mute": {
      "patch": {
        "operationId": "PushNotificationApi_Mute",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/notificationapiMuteReply"
            }
          },
          "400": {
            "description": "Could not parse resource trn.",
            "schema": {}
          },
          "403": {
            "description": "You don't have access to the specified resource.",
            "schema": {}
          },
          "404": {
            "description": "Could not find resource.",
            "schema": {}
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/notificationapiMuteRequest"
            }
          }
        ],
        "tags": [
          "PushNotificationApi"
        ]
      }
    },
    "/push-notification-api/resources/machines/muted": {
      "get": {
        "operationId": "PushNotificationApi_ListMutedMachines",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/notificationapiListMutedMachinesResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "tags": [
          "PushNotificationApi"
        ]
      }
    },
    "/push-notification-api/resources/{machineId}/mute_info": {
      "get": {
        "operationId": "PushNotificationApi_GetMutedMachineSettings",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/notificationapiGetMutedResourceResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "machineId",
            "description": "trn of the machine",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "subType",
            "description": "type of the subresource to check for mute settings",
            "in": "query",
            "required": false,
            "type": "string"
          },
          {
            "name": "subId",
            "description": "id of the subresource to check for mute settings",
            "in": "query",
            "required": false,
            "type": "string"
          }
        ],
        "tags": [
          "PushNotificationApi"
        ]
      }
    },
    "/push-notification-api/resources/{machineId}/{subType}/{subId}/mute_info": {
      "get": {
        "operationId": "PushNotificationApi_GetMutedSubResourceSettings",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/notificationapiGetMutedResourceResponse"
            }
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "machineId",
            "description": "trn of the machine",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "subType",
            "description": "type of the subresource to check for mute settings",
            "in": "path",
            "required": true,
            "type": "string"
          },
          {
            "name": "subId",
            "description": "id of the subresource to check for mute settings",
            "in": "path",
            "required": true,
            "type": "string"
          }
        ],
        "tags": [
          "PushNotificationApi"
        ]
      }
    },
    "/push-notification-api/unmute": {
      "patch": {
        "operationId": "PushNotificationApi_Unmute",
        "responses": {
          "200": {
            "description": "A successful response.",
            "schema": {
              "$ref": "#/definitions/notificationapiUnmuteReply"
            }
          },
          "400": {
            "description": "Could not parse resource trn.",
            "schema": {}
          },
          "403": {
            "description": "You don't have access to the specified resource.",
            "schema": {}
          },
          "404": {
            "description": "Could not find resource.",
            "schema": {}
          },
          "default": {
            "description": "An unexpected error response.",
            "schema": {
              "$ref": "#/definitions/rpcStatus"
            }
          }
        },
        "parameters": [
          {
            "name": "body",
            "in": "body",
            "required": true,
            "schema": {
              "$ref": "#/definitions/notificationapiUnmuteRequest"
            }
          }
        ],
        "tags": [
          "PushNotificationApi"
        ]
      }
    }
  },
  "definitions": {
    "ListMutedMachinesResponseMuted_machine": {
      "type": "object",
      "properties": {
        "machineId": {
          "type": "string",
          "description": "trn of the muted machine"
        },
        "mutedEmergencyOnly": {
          "type": "boolean",
          "description": "signifies if muted resource allows emergency notifications or not"
        },
        "isMutedIndefinitely": {
          "type": "boolean",
          "description": "signifies if requested resource is muted indefinitely or not"
        },
        "mutedUntil": {
          "type": "string",
          "format": "int64",
          "description": "if is_muted_indefinitely equals false, expiration for the muted resource in UNIX (seconds)"
        }
      }
    },
    "notificationapiCheckUserDeviceRegistrationReply": {
      "type": "object",
      "properties": {
        "registrationSuccessful": {
          "type": "boolean"
        }
      }
    },
    "notificationapiDeregisterUserDeviceReply": {
      "type": "object"
    },
    "notificationapiDeregisterUserDeviceRequest": {
      "type": "object",
      "properties": {
        "registrationToken": {
          "type": "string",
          "description": "device registration token"
        }
      }
    },
    "notificationapiGetMutedResourceResponse": {
      "type": "object",
      "properties": {
        "isMuted": {
          "type": "boolean",
          "description": "signifies if requested resource is muted or not"
        },
        "mutedEmergencyOnly": {
          "type": "boolean",
          "description": "signifies if muted resource allows emergency notifications or not"
        },
        "isMutedIndefinitely": {
          "type": "boolean",
          "description": "signifies if requested resource is muted indefinitely or not"
        },
        "mutedUntil": {
          "type": "string",
          "format": "int64",
          "description": "if is_muted_indefinitely equals false, expiration for the muted resource in UNIX (seconds)"
        }
      }
    },
    "notificationapiListMutedMachinesResponse": {
      "type": "object",
      "properties": {
        "mutedMachines": {
          "type": "array",
          "items": {
            "type": "object",
            "$ref": "#/definitions/ListMutedMachinesResponseMuted_machine"
          },
          "description": "machine IDs of all machines that are currently muted for the user making the request, including expiration timestamps"
        }
      }
    },
    "notificationapiMuteReply": {
      "type": "object",
      "properties": {
        "isMuted": {
          "type": "boolean",
          "description": "signifies if requested resource is muted or not"
        },
        "mutedEmergencyOnly": {
          "type": "boolean",
          "description": "signifies if muted resource allows emergency notifications or not"
        },
        "isMutedIndefinitely": {
          "type": "boolean",
          "description": "signifies if requested resource is muted indefinitely or not"
        },
        "mutedUntil": {
          "type": "string",
          "format": "int64",
          "description": "if is_muted_indefinitely equals false, expiration for the muted resource in UNIX (seconds)"
        }
      }
    },
    "notificationapiMuteRequest": {
      "type": "object",
      "properties": {
        "targetTrn": {
          "type": "string",
          "description": "trn of the resource to mute"
        },
        "until": {
          "$ref": "#/definitions/notificationapiMuteUntil",
          "description": "enum designating for how long to mute"
        },
        "setEmergencyOnly": {
          "type": "boolean",
          "description": "toggle indicating whether to still receive emergency notifications"
        }
      }
    },
    "notificationapiMuteUntil": {
      "type": "string",
      "enum": [
        "Always",
        "EightHours",
        "OneHour"
      ],
      "default": "Always"
    },
    "notificationapiRegisterUserDeviceReply": {
      "type": "object"
    },
    "notificationapiRegisterUserDeviceRequest": {
      "type": "object",
      "properties": {
        "registrationToken": {
          "type": "string",
          "description": "device registration token"
        }
      }
    },
    "notificationapiSendNotificationReply": {
      "type": "object"
    },
    "notificationapiUnmuteReply": {
      "type": "object"
    },
    "notificationapiUnmuteRequest": {
      "type": "object",
      "properties": {
        "targetTrn": {
          "type": "string",
          "description": "trn of the resource to unmute"
        }
      }
    },
    "protobufAny": {
      "type": "object",
      "properties": {
        "@type": {
          "type": "string"
        }
      },
      "additionalProperties": {}
    },
    "rpcStatus": {
      "type": "object",
      "properties": {
        "code": {
          "type": "integer",
          "format": "int32"
        },
        "message": {
          "type": "string"
        },
        "details": {
          "type": "array",
          "items": {
            "type": "object",
            "$ref": "#/definitions/protobufAny"
          }
        }
      }
    }
  },
  "securityDefinitions": {
    "ApiKeyAuth": {
      "type": "apiKey",
      "name": "X-API-Key",
      "in": "header"
    }
  },
  "security": [
    {
      "ApiKeyAuth": []
    }
  ],
  "externalDocs": {
    "description": "service repository",
    "url": "https://bitbucket.org/innius/push-notification-api"
  }
}
`
)
